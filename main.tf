provider "aws" {
    profile = "default"
  region  = "eu-central-1"
}

data "aws_ami" "ubuntu" {
    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
    }

    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"]
}
resource "aws_instance" "learning_test_ec2" {
  ami = data.aws_ami.ubuntu.image_id
  key_name = aws_key_pair.anita.key_name 
  instance_type = "t2.micro"
  user_data = file("userdata.sh")
  
  vpc_security_group_ids = ["${aws_security_group.development_ani21ta_sgroup.id}"]
  
  tags = {
    Name = "learning_test_ec2"
  }
}

resource "aws_key_pair" "anita" {
  key_name = "anita-key"
  public_key = file(pathexpand("~/.ssh/id_rsa.pub"))
}
#S3 Bucket
resource "aws_s3_bucket" "development_ani21ta-bucket" {
  bucket = "ani21ta-bucket"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
   
   tags = {
    Name = "ani21ta-bucket"
    Environment = "Dev"
  }
} 

#IAM Role
resource "aws_iam_role" "development_iamrole-anita" {
  name = "development_iamrole-anita"
  assume_role_policy = file("s3_iamrolename.json")
}
resource "aws_iam_instance_profile" "development_learning_test_ec2" {
  name = "development_learning_test_ec2"
  role = aws_iam_role.development_iamrole-anita.name
}

#IAM Role policy
resource "aws_iam_role_policy" "development_s3_iampolicy" {
  name = "development_learning_test_ec2"
  role = aws_iam_role.development_iamrole-anita.id
  policy = file("s3_iampolicy.json")
}

resource "aws_security_group" "development_ani21ta_sgroup" {
  name = "development_ani21ta_sgroup"

  ingress {
    
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["78.84.166.239/32"]
  }
 
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Anita-security-group"
  }
}
# resource "aws_instance" "learning_test_ec2" {
#   ami = data.aws_ami.ubuntu.image_id
#   key_name = aws_key_pair.anita.key_name
#   instance_type = "t2.micro"
#   user_data = file("userdata.sh")
#   tags = {
#     Name = "learning_test_ec2"
#   }
# }
output "instance_public_ip" {
 value = aws_instance.learning_test_ec2.public_ip
}
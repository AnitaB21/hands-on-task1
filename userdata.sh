#! /bin/bash -x
# change ssh port to 443
sudo sed -i 's/Port 22/Port 443/g' /etc/ssh/sshd_config
sudo sed -i 's/#Port 443/Port 443/g' /etc/ssh/sshd_config
sudo service ssh restart
apt update -y
apt install software-properties-common -y
apt-add-repository --yes --update ppa:ansible/ansible
apt install -y ansible

apt install -y git

apt install -y awscli

git clone https://gitlab.com/AnitaB21/hands-on-task1.git

ansible-playbook --connection=local --inventory 127.0.0.1, hands-on-task1/ansible_playbook.yml

nginx -v 2>> home/ubuntu/output.txt